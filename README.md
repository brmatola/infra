# README

This is a bootstrap for a GCP project. Clone and start [... more]

## Setup

- Make new project and open the cloud console
- Generate SSH Keys and add to repository host (GitHub, BitBucket, etc.) using:
- ssh-keygen -t rsa -b 4096 -C "myemail@email.com"
- Clone this repo
- Add the source repo for infrastructure after forking from the build starter in this project
- Update the variables section of this repo to reflect that
- run terraform apply
