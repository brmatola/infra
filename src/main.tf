resource "google_project_service" "cloud_build" {
    project = var.project
    service = "cloudbuild.googleapis.com"
    disable_dependent_services = true
}

resource "google_project_service" "iam" {
    project = var.project
    service = "iam.googleapis.com"
    disable_dependent_services = true
}

resource "google_project_service" "cloud_source" {
    project = var.project
    service = "sourcerepo.googleapis.com"
    disable_dependent_services = true
}

data "google_sourcerepo_repository" "infrastructure" {
    name = var.infrastructure_repo
}

resource "google_cloudbuild_trigger" "infra_trigger" {
    name = "Infrastructure"
    description = "Triggers build to add basic infrastructure to project"
    service_account = "projects/${var.project}/serviceAccounts/${google_service_account.builder.email}"
    filename = "cloudbuild.yaml"

    trigger_template {
      branch_name = "main"
      repo_name = data.google_sourcerepo_repository.infrastructure.name
    }

    substitutions = {
        _LOG_BUCKET_URL = google_storage_bucket.build_logs.url
        _TF_STATE_BUCKET_NAME = google_storage_bucket.tf_state.name
    }

    depends_on = [google_project_service.cloud_build]
}

resource "google_service_account" "builder" {
    account_id = "infrastructure-builder"
    display_name = "Infrastructure Builder"
}

resource "google_project_iam_custom_role" "infra_builder" {
    role_id = "iamManager"
    title = "IAM Manager"
    description = "Runs builds for infrastructure"
    permissions = [
        "resourcemanager.projects.get", 
        "resourcemanager.projects.getIamPolicy", 
        "resourcemanager.projects.setIamPolicy",
        "source.repos.get"
    ]
}

resource "google_project_iam_binding" "builder" {
    project = var.project
    role = google_project_iam_custom_role.infra_builder.id
    members = ["serviceAccount:${google_service_account.builder.email}"]
}

resource "google_storage_bucket" "tf_state" {
    name = "${var.project}-tf-state"
    location = "US"
}

resource "google_storage_bucket" "build_logs" {
    name = "${var.project}-build-logs"
    location = "US"
}

resource "google_storage_bucket_iam_binding" "admin" {
    bucket = google_storage_bucket.build_logs.name
    role = "roles/storage.admin" #TODO: too high?
    members = ["serviceAccount:${google_service_account.builder.email}"]
}